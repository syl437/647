import {Component, Injector, ViewChild} from '@angular/core';
import {AlertController, Nav, NavController, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {HomePage} from '../pages/home/home';
import {ListPage} from '../pages/list/list';
import {RegisterPage} from "../pages/register/register";
import {TrainingsPage} from "../pages/trainings/trainings";
import {AddTrainingPage} from "../pages/add-training/add-training";
import {WeightPage} from "../pages/weight/weight";
import {QuestionsPage} from "../pages/questions/questions";
import {MealsPage} from "../pages/meals/meals";
import {MealsMenuPage} from "../pages/meals-menu/meals-menu";
import {AboutPage} from "../pages/about/about";
import {ChatPage} from "../pages/chat/chat";
import {LoginPage} from "../pages/login/login";
import {Push, PushObject, PushOptions} from "@ionic-native/push";
import {Config} from "../services/config";
import {ChatSevice} from "../services/chatService";
import {Events} from 'ionic-angular';
import {Badge} from '@ionic-native/badge';
import {get} from "@ionic-native/core";
import {FCM} from "@ionic-native/fcm";
import {Firebase} from "@ionic-native/firebase";
import {loginService} from "../services/loginService";
import {WeightService} from "../services/weightService";
import {TrainingsSevice} from "../services/TraingngsService";


//declare var FCMPlugin;

@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    @ViewChild('myNav') nav: NavController

    rootPage: any = HomePage;
    Obj: any;
    public date: any;
    public hours: any;
    public minutes: any;
    public seconds: any;
    public time: any;
    public today: any;
    public dd: any;
    public mm: any;
    public yyyy: any;
    public newdate: any;
    pages: Array<{ title: string, component: any }>;

    constructor( protected injector: Injector, private trainingService: TrainingsSevice, private weightService: WeightService, private loginService: loginService, private firebase: Firebase, public alertCtrl: AlertController, public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, private push: Push, public Settings: Config, public ChatService: ChatSevice, public events: Events, private badge: Badge) {
        this.initializeApp();

        // used for an example of ngFor and navigation
        this.pages = [
            {title: 'Home', component: HomePage},
            {title: 'List', component: ListPage}
        ];
    }

    get navCtrl(): NavController {
        return this.injector.get(NavController);
    }

    initializeApp() {
        this.platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            this.statusBar.styleDefault();
            var d = new Date();
            var dayName = d.getDay();
            var token = '';


            if (window.localStorage.identify != null && window.localStorage.identify != '')
            {
                if (dayName == 0) {
                    this.getTraining();
                }
                console.log("Today : ", dayName)
    
                this.checkYesterday();
                this.checkWeight();

                this.loginService.GetToken('GetToken',window.localStorage.push_id).then((data: any) => {

                });
            }
           

            this.splashScreen.hide();
            if (this.platform.is('cordova')) {

                if (this.platform.is('android')) {
                    this.initializeFireBaseAndroid();
                }
                if (this.platform.is('ios')) {
                    this.initializeFireBaseIos();
                }
            }

            // this.navCtrl.push(ChatPage);
            //this.pushSetup();
            this.badge.clear();


            this.platform.registerBackButtonAction(() => {

                if (this.nav.length() == 1) {

                    let alert = this.alertCtrl.create({
                        title: 'Exit',
                        message: 'Do you want to exit?',
                        buttons: [
                            {
                                text: "OK", handler: () => {
                                this.platform.exitApp();
                            }
                            },
                            {text: "Cancel", role: 'cancel'}
                        ]
                    });
                    alert.present();

                } else
                    this.nav.pop();

            });
        });
    }

    presentAlert(title , message) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: message,
            buttons: ['אשר'],
            cssClass: 'alertCustomCss'
        });
        alert.present();
    }

    private checkWeight() {
        var d = this.getDate()

        if (d != window.localStorage.weightDate) {
            this.weightService.checkWeight('checkWeight').then((data: any) => {


                if (Number(data) > 0) {
                    this.presentAlert(    "ירידה במשקל" , "כל הכבוד בשבוע שעבר ירדת  " + Number(data) + " ממשקלך !! כל הכבוד !!")
                    //alert("כל הכבוד בשבוע שעבר ירדת  " + Number(data) + " ממשקלך !! כל הכבוד !!")
                }
                else if (Number(data) == 0) {
                    this.presentAlert( "ירידה במשקל" , " שימי לב בשבוע שעבר עלית במשקל ב :  " + Number(data))
                    //alert(" שימי לב בשבוע שעבר עלית במשקל ב :  " + Number(data))
                }
                else if (Number(data) < 0) {
                    this.presentAlert( "ירידה במשקל" , " שימי לב בשבוע שעבר עלית במשקל ב :  " + Number(data))
                    //alert(" שימי לב בשבוע שעבר עלית במשקל ב :  " + Number(data))
                }

                window.localStorage.weightDate = d;

            });
        }
    }

    public getDate() {
        var today: any = new Date();
        var dd: any = today.getDate();
        var mm: any = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        if (Number(dd) < 10) {
            dd = '0' + dd.toString();
        }
        if (mm < 10) {
            mm = '0' + mm;
        }

        today = dd + '/' + mm + '/' + yyyy;

        return today;
    }


    private getTraining() {

        var d = this.getDate()

        if (d != window.localStorage.trainingDate) {
            this.trainingService.checkWeaklyTraining('checkWeaklyTraining').then((data: any) => {
                console.log("checkWeaklyTraining : ", data);

                if (Number(data) > 3) {
                    //alert("כל הכבוד בשבוע שעבר ביצעת  " + Number(data) + " אימונים !! כל הכבוד !!")
                    this.presentAlert(    "מספר אימונים בשבוע שעבר" , "כל הכבוד בשבוע שעבר ביצעת  " + Number(data) + " אימונים !! כל הכבוד !!")
                }
                else if (Number(data) == 2) {
                    //alert("הצלחתך בתכנית דורשת 4 אימונים בשבוע , בשבוע שעבר בוצעו רק  " + Number(data) + 'אימונים');
                    this.presentAlert(    "מספר אימונים בשבוע שעבר" , "הצלחתך בתכנית דורשת 4 אימונים בשבוע , בשבוע שעבר בוצעו רק  " + Number(data) + 'אימונים')
                }
                else if (Number(data) == 1) {
                    //alert("הצלחתך בתכנית דורשת 4 אימונים בשבוע , בשבוע שעבר בוצע רק אימון אחד ");
                    this.presentAlert(    "מספר אימונים בשבוע שעבר" ,  "הצלחתך בתכנית דורשת 4 אימונים בשבוע , בשבוע שעבר בוצע רק אימון אחד ")
                }
                else if (Number(data) == 0) {
                    //alert("הצלחתך בתכנית דורשת 4 אימונים בשבוע , בשבוע שעבר לא ביצעת אימונים בכלל ");
                    this.presentAlert(    "מספר אימונים בשבוע שעבר" , "הצלחתך בתכנית דורשת 4 אימונים בשבוע , בשבוע שעבר לא ביצעת אימונים בכלל ")
                }

                window.localStorage.trainingDate = d;

            });
        }
    }

    private checkYesterday() {
        this.loginService.checkYesterday('checkYesterday').then((data: any) => {
            console.log("Data : ", data);

            if (data == 0) {
                //alert("שים לב !! אתמול לא דיווחת ארוחות בכלל חשוב לזכור שההצלחה בתוכנית מחייבת ישום מלא של השיטה");
                this.presentAlert("מספר ארוחות", "שים לב !! אתמול לא דיווחת ארוחות בכלל חשוב לזכור שההצלחה בתוכנית מחייבת ישום מלא של השיטה")
            }
            else if (data < 6 && data > 0)
            {
                //alert("שים לב !! אתמול דיווחת רק " + data + " ארוחות חשוב לזכור שההצלחה בתוכנית מחייבת ישום מלא של השיטה");
                this.presentAlert(    "מספר ארוחות" , "שים לב !! אתמול דיווחת רק " + data + " ארוחות חשוב לזכור שההצלחה בתוכנית מחייבת ישום מלא של השיטה")
            }
            else if (data >= 6)
            {
                //alert("כל הכבוד !! אתמול דיווחת 6 ארוחות אתה בדרך לשינוי");
                this.presentAlert(    "מספר ארוחות" ,"כל הכבוד !! אתמול דיווחת 6 ארוחות אתה בדרך לשינוי")
            }

        });
    }

    private initializeFireBaseAndroid(): Promise<any> {
        return this.firebase.getToken()
            .catch(error => console.error('Error getting token', error))
            .then(token => {

                this.firebase.subscribe('all').then((result) => {
                    if (result) console.log(`Subscribed to all`);
                    this.subscribeToPushNotificationEvents();
                });
            });
    }

    private initializeFireBaseIos(): Promise<any> {
        return this.firebase.grantPermission()
            .catch(error => console.error('Error getting permission', error))
            .then(() => {
                this.firebase.getToken()
                    .catch(error => console.error('Error getting token', error))
                    .then(token => {

                        console.log(`The token is ${token}`);

                        this.firebase.subscribe('all').then((result) => {
                            if (result) console.log(`Subscribed to all`);

                            this.subscribeToPushNotificationEvents();
                        });
                    });
            })

    }

    private saveToken(token: any): Promise<any> {
        // Send the token to the server
        // console.log('Sending token to the server...');
        return Promise.resolve(true);
    }

    private subscribeToPushNotificationEvents(): void {

        // Handle token refresh
        this.firebase.onTokenRefresh().subscribe(
            token => {
                this.Settings.SetUserPush(token);
                this.saveToken(token);
                this.loginService.sendToken('GetToken', token).then((data: any) => {
                    console.log("UserDetails : ", data);
                });
            },
            error => {
                console.error('Error refreshing token', error);
            });

        // Handle incoming notifications
        this.firebase.onNotificationOpen().subscribe(
            (notification) => {

                // !notification.tap
                //     ? alert('The user was using the app when the notification arrived...')
                //     : alert('The app was closed when the notification arrived...');


                if (!notification.tap) {
                    //alert(JSON.stringify(notification));
                    this.date = new Date()
                    this.hours = this.date.getHours()
                    this.minutes = this.date.getMinutes()
                    this.seconds = this.date.getSeconds()

                    if (this.hours < 10)
                        this.hours = "0" + this.hours

                    if (this.minutes < 10)
                        this.minutes = "0" + this.minutes
                    this.time = this.hours + ':' + this.minutes;


                    this.today = new Date();
                    this.dd = this.today.getDate();
                    this.mm = this.today.getMonth() + 1; //January is 0!
                    this.yyyy = this.today.getFullYear();

                    if (this.dd < 10) {
                        this.dd = '0' + this.dd
                    }

                    if (this.mm < 10) {
                        this.mm = '0' + this.mm
                    }

                    this.today = this.dd + '/' + this.mm + '/' + this.yyyy;
                    this.newdate = this.today + ' ' + this.time;


                    //alert("Notification " + notification.message );
                    this.Obj = {
                        id: '',
                        uid: 3,
                        title: notification.message,
                        date: this.newdate,
                        type: notification.type,
                        name: notification.fullname,
                        image: notification.image
                    };
                    this.ChatService.pushToArray(this.Obj);
                    this.events.publish('newchat', "newchat");
                }
                else {
                    //alert(JSON.stringify(notification));

                    if (window.localStorage.identify && notification.type == '1' || notification.type == '3') {
                        this.nav.push(ChatPage);
                    }
                }
                // let notificationAlert = this.alertCtrl.create({
                //     title: notification.title,
                //     message: JSON.stringify(notification),
                //     buttons: ['Ok']
                // });
                // notificationAlert.present();
            },
            error => {
                console.error('Error getting the notification', error);
            });
    }


    openPage(page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    }


    pushSetup() {
        // to initialize push notifications

        const options: PushOptions = {
            android: {
                senderID: '1034167244395',
            },
            ios: {
                alert: 'true',
                badge: true,
                sound: 'false'
            },
            windows: {},
            browser: {
                pushServiceURL: 'http://push.api.phonegap.com/v1/push'
            }
        };

        const pushObject: PushObject = this.push.init(options);

        pushObject.on('notification').subscribe((notification: any) => {

            //alert("Notification222 " + JSON.stringify(notification));
            if (notification.additionalData.foreground) {
                alert("p1")
                this.date = new Date()
                this.hours = this.date.getHours()
                this.minutes = this.date.getMinutes()
                this.seconds = this.date.getSeconds()

                if (this.hours < 10)
                    this.hours = "0" + this.hours

                if (this.minutes < 10)
                    this.minutes = "0" + this.minutes
                this.time = this.hours + ':' + this.minutes;


                this.today = new Date();
                this.dd = this.today.getDate();
                this.mm = this.today.getMonth() + 1; //January is 0!
                this.yyyy = this.today.getFullYear();

                if (this.dd < 10) {
                    this.dd = '0' + this.dd
                }

                if (this.mm < 10) {
                    this.mm = '0' + this.mm
                }

                this.today = this.dd + '/' + this.mm + '/' + this.yyyy;
                this.newdate = this.today + ' ' + this.time;


                //alert("Notification " + notification.message );
                this.Obj = {
                    id: '',
                    uid: 3,
                    title: notification.message,
                    date: this.newdate,
                    type: 1,
                    name: notification.additionalData.fullname,
                    image: notification.image
                };
                this.ChatService.pushToArray(this.Obj);
                this.events.publish('newchat', "newchat");
            }
            else {
                if (window.localStorage.identify) {
                    if (notification.type == 1)
                        this.nav.push(ChatPage);
                    else if (notification.type == 2)
                        this.nav.push(MealsPage);
                }
            }
        });

        pushObject.on('registration').subscribe((registration: any) => {
            this.Settings.SetUserPush(registration.registrationId.toString());
            alert("registration")
        });


        //pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
    }


}
