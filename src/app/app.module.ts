import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';

import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {ListPage} from '../pages/list/list';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {RegisterPage} from "../pages/register/register";

import {AddTrainingPage} from "../pages/add-training/add-training";
import {Http, HttpModule} from "@angular/http";
import {TrainingsSevice} from "../services/TraingngsService";
import {TrainingModel} from "../models/TrainingModel";
import {Config} from "../services/config";
import {WeightPage} from "../pages/weight/weight";
import {SinglGroupWeightPage} from "../pages/singl-group-weight/singl-group-weight";
import {WeightService} from "../services/weightService";
import {QuestionsPage} from "../pages/questions/questions";
import {QuestionService} from "../services/QuestionService";
import {MealsMenuPage} from "../pages/meals-menu/meals-menu";
import {MealsPage} from "../pages/meals/meals";
import {MealsService} from "../services/MealsService";
import {TrainingsPage} from "../pages/trainings/trainings";
import {AboutPage} from "../pages/about/about";
import {LoginPage} from "../pages/login/login";
import {loginService} from "../services/loginService";
import {ChatSevice} from "../services/chatService";
import {ChatPage} from "../pages/chat/chat";
import {Push} from "@ionic-native/push";
import {Badge} from '@ionic-native/badge';
import {PopupPage} from "../pages/popup/popup";
import {FCM} from "@ionic-native/fcm";
import {Firebase} from "@ionic-native/firebase";
import {DayAfterPage} from "../pages/day-after/day-after";
import { AutosizeDirective } from '../directives/autosize/autosize';

import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';
import { FilePath } from '@ionic-native/file-path';
import { ChatModalPage } from '../pages/chat-modal/chat-modal';
import { ZoomAreaModule } from 'ionic2-zoom-area';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IonicImageViewerModule } from 'ionic-img-viewer';



@NgModule({
    declarations: [
        MyApp,
        HomePage,
        ListPage,
        RegisterPage,
        TrainingsPage,
        AddTrainingPage,
        WeightPage,
        SinglGroupWeightPage,
        QuestionsPage,
        MealsPage,
        MealsMenuPage,
        PopupPage,
        AboutPage,
        DayAfterPage,
        LoginPage,
        ChatPage,
        AutosizeDirective,
        ChatModalPage
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyApp),
        HttpModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpModule,
        ZoomAreaModule.forRoot(),
        IonicImageViewerModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        ListPage,
        RegisterPage,
        TrainingsPage,
        AddTrainingPage,
        WeightPage,
        SinglGroupWeightPage,
        QuestionsPage,
        DayAfterPage,
        MealsPage,
        MealsMenuPage,
        PopupPage,
        AboutPage,
        LoginPage,
        ChatPage,
        ChatModalPage
    ],
    providers: [
        StatusBar,
        Firebase,
        AutosizeDirective,
        SplashScreen,
        FileTransfer,
        FileTransferObject,
        File,
        Camera,
        FilePath,

        {
            provide: ErrorHandler,
            useClass: IonicErrorHandler
        }, TrainingsSevice, TrainingModel, Config, WeightService, QuestionService, MealsService, loginService, ChatSevice, Push, Badge,
    ]
})
export class AppModule {
}
