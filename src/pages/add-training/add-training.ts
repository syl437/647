import {Component, OnInit} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {FormControl, FormGroup, NgForm, Validators} from "@angular/forms";
import {TrainingsSevice} from "../../services/TraingngsService";
import {TrainingsPage} from "../trainings/trainings";


@IonicPage()
@Component({
  selector: 'page-add-training',
  templateUrl: 'add-training.html',
})
export class AddTrainingPage  implements OnInit{

  TrainingsTypes = [];
  isDataAvailable:boolean = false;
  trainingsForm: FormGroup;
  time : string = ''; type : string = ''; date : string = '';

  constructor(public navCtrl: NavController, public navParams: NavParams, public TrainingsService:TrainingsSevice) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddTrainingPage');
  }

  ngOnInit() {
    this.TrainingsService.getTrainigTypes().then((data: any) => {this.TrainingsTypes  = data , console.log("Types12 : " , this.TrainingsTypes[0].title),this.isDataAvailable = true;})
  }



  onSubmit(form:NgForm)
  {
    this.TrainingsService.addTraining("addTraining" , form.value.time , form.value.date , form.value.type).then((data: any) => {console.log("DT2 : " , data); this.navCtrl.push(TrainingsPage)})
    form.reset();
  }
}
