import { Component,OnInit } from '@angular/core';
import {IonicPage, NavController, NavParams, Platform, ViewController} from 'ionic-angular';
import {Config} from "../../services/config";

/**
 * Generated class for the ChatModalPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-chat-modal',
  templateUrl: 'chat-modal.html',
})
export class ChatModalPage implements OnInit {

    imagePath: string = this.navParams.get('imagePath');
    public phpHost;
    isAndroid:boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController,public Settings: Config,public platform: Platform) {

      this.phpHost = Settings.phpHost;

      if (this.platform.is('android')) {
          this.isAndroid = true;
      }

          platform.registerBackButtonAction(() => {
          this.viewCtrl.dismiss();
      },1);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatModalPage');
  }

    ngOnInit() {

    }

    closeImageModal(){
        this.viewCtrl.dismiss();
    }



}
