import {Inject , Component, ElementRef, NgZone, OnInit, ViewChild} from '@angular/core';
import {Events, IonicPage, LoadingController, ModalController, NavController, NavParams} from 'ionic-angular';
import {ChatSevice} from "../../services/chatService";
import {Content} from "ionic-angular";
import {Platform , TextInput, ActionSheetController} from 'ionic-angular';
import {Config} from "../../services/config";
import { DOCUMENT } from '@angular/platform-browser';
import { AutosizeDirective } from '../../directives/autosize/autosize';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { ChatModalPage } from '../../pages/chat-modal/chat-modal';

declare var cordova: any;
//constructor(@Inject(DOCUMENT) private document: Document) { }

/**
 * Generated class for the ChatPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: 'page-chat',
    templateUrl: 'chat.html',
})
export class ChatPage implements OnInit {
    @ViewChild(Content) content: Content;
    @ViewChild('scrollMe') private myScrollContainer: ElementRef;

    public chatText = '';
    public ChatArray = [];
    Obj: any;
    public screenHeight = screen.height - 100 + 'px';
    public date: any;
    public hours: any;
    public minutes: any;
    public seconds: any;
    public time: any;
    public today: any;
    public dd: any;
    public mm: any;
    public yyyy: any;
    public newdate: any;
    public phpHost;
    public ServerUrl;
    innerHeight: any;
    loading:any;
    rowNum = 1;
    TextAreaContent:any;
    tArea:any;
    correctPath;
    currentName;
    lastImage: string = null;
    uploadedImagePath:any;

    constructor(public navCtrl: NavController,public element:ElementRef,  public loadingCtrl: LoadingController, public zone: NgZone, public navParams: NavParams, public ChatService: ChatSevice, public platform: Platform, public Settings: Config, public events: Events,public actionSheet: ActionSheetController,private camera: Camera,private file: File,private transfer: FileTransfer,private filePath: FilePath,public modalCtrl: ModalController) {
        this.ChatService._ChatArray.subscribe(val => {
            this.zone.run(() => {
                this.ChatArray = val;
            });
        });
        this.phpHost = Settings.phpHost;
        this.ServerUrl = Settings.ServerUrl;
        document.addEventListener('resume', () => {
            this.getChatDetails();
        });


         if(typeof this.loading != "undefined")
         this.loading.dismiss();

         this.loading = this.loadingCtrl.create({
             content: 'עוד רגע מתחילים'
         });



        events.subscribe('newchat', (user, time) => {
            // user and time are the same arguments passed in `events.publish(user, time)`
            this.scrollBottom();
        });

        //this.addTextArea()
    }


    ngOnInit() {
        this.getChatDetails();
        this.scrollBottom();

        this.innerHeight = (window.screen.height);
        let elm = <HTMLElement>document.querySelector(".chatContent");
        elm.style.height = (this.innerHeight - (this.innerHeight * 0.23)) + 'px'
    }


    addTextArea()
    {
        this.TextAreaContent = document.getElementById('TextContent');
        console.log("TA : " + this.TextAreaContent)
        this.TextAreaContent.innerHTML = '<ion-textarea class="chatInput" id="chatInput" rows="1"    autosize placeholder="הוסף הודעה" name="title"  [(ngModel)]="chatText" (keypress)="enterPress($event)"></ion-textarea>'
    }

    scrollBottom() {
        setTimeout(() => {
            //var objDiv = <HTMLElement>document.querySelector(".chatContent");
            //objDiv.scrollTop = objDiv.scrollHeight;

            var element = document.getElementById("chatContent");
            element.scrollIntoView(true);
            element.scrollTop = element.scrollHeight;
            this.content.scrollToBottom(0);
        }, 300);
    }


    scrollToBottom(): void {
        try {
            this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
        } catch(err) { }
    }


    getChatDetails() {
        //this.loading.present();

        this.ChatService.getChatDetails('getChatDetailsForTest').then((data: any) => {
            //this.loading.dismiss();
            console.log("ChatDetails: ", data), this.ChatArray = data.reverse() , this.scrollBottom();
        });
    }

    ionViewDidLoad() {
        this.scrollBottom();
        console.log("TP : " , this.loading)
        //if(typeof this.loading != "undefined")
        //this.loading.dismiss();
    }

    ionViewWillEnter() {
        this.scrollBottom();
        //if(typeof this.loading != "undefined")
        //this.loading.dismiss();
    }

    addChatTitle(isImage:any) {

        let chatText1 = '';
        let chatType = '';

        if (isImage == 1)
        {
             chatText1 = this.uploadedImagePath;
             chatType = '3';
        }
        else
        {
            chatText1 = this.chatText;
            chatType = '1';
        }



        if (chatText1)
        {
            this.chatText = '';
            this.date = new Date()
            this.hours = this.date.getHours()
            this.minutes = this.date.getMinutes()
            this.seconds = this.date.getSeconds()

            if (this.hours < 10)
                this.hours = "0" + this.hours

            if (this.minutes < 10)
                this.minutes = "0" + this.minutes
            this.time = this.hours + ':' + this.minutes;


            this.today = new Date();
            this.dd = this.today.getDate();
            this.mm = this.today.getMonth() + 1; //January is 0!
            this.yyyy = this.today.getFullYear();

            if (this.dd < 10) {
                this.dd = '0' + this.dd
            }

            if (this.mm < 10) {
                this.mm = '0' + this.mm
            }

            this.today = this.dd + '/' + this.mm + '/' + this.yyyy;
            this.newdate = this.today + ' ' + this.time;


            this.Obj = {
                id: '',
                uid: window.localStorage.identify,
                name: window.localStorage.name,
                title: chatText1,
                date: this.newdate,
                type: chatType,
                image: window.localStorage.user_image
            };

            this.ChatService.pushToArray(this.Obj);
            this.ChatService.addTitle('addChatTitle', chatText1, this.newdate,window.localStorage.name, window.localStorage.user_image,chatType).then((data: any) => {
                console.log("Weights : ", data), chatText1 = '', this.scrollBottom(),this.uploadedImagePath = '';
            });

            this.autoSize(40);
            //let txtArea = <HTMLElement>document.querySelector(".TextContent")
            //txtArea.innerHTML = '<ion-textarea class="chatInput" id="myInput" rows="1"    autosize placeholder="הוסף הודעה" name="title"  [(ngModel)]="chatText" (keypress)="enterPress($event)" #myInput></ion-textarea>';
            //txtArea.style.height = "auto";
        }




    }

    enlargeImage(newimage) {
        /*
        let modalObj = {imagePath: newimage };
        let ChatImageModal = this.modalCtrl.create(ChatModalPage, modalObj);
        ChatImageModal.present();
        */
    }

    // autoSize(pixels)
    // {
    //     let textArea = this.element.nativeElement.getElementsByTagName('title')[0];
    //     console.log(textArea)
    //     textArea.style.overflow = 'hidden';
    //     textArea.style.height  = 'auto';
    //     if (pixels === 0){
    //         textArea.style.height  = textArea.scrollHeight + 'px';
    //     } else {
    //         textArea.style.height  = pixels + 'px';
    //     }
    //     return;
    // }


    photoOptions() {
        let actionSheet = this.actionSheet.create({
            title: 'בחירת מקור התמונה',
            buttons: [
                {
                    text: 'גלריית תמונות',
                    icon: 'albums',
                    handler: () => {
                        this.takePhoto(this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'מצלמה',
                    icon: 'camera',
                    handler: () => {
                        this.takePhoto(this.camera.PictureSourceType.CAMERA);
                    }
                }, {
                    text: 'ביטול',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });

        actionSheet.present();
    }


    public pathForImage(img) {
        if (img === null) {
            return '';
        } else {
            return cordova.file.dataDirectory + img;
        }
    }


    uploadPhoto() {


        // Destination URL
        //var url = "http://tapper.org.il/647/laravel/public/api/uploadImage";
        var targetPath = this.pathForImage(this.lastImage);

        // File name only
        var filename = this.lastImage;

        var options = {
            fileKey: "file",
            fileName: filename,
            chunkedMode: false,
            mimeType: "multipart/form-data",
            params : {'fileName': filename}
        };

        const fileTransfer: FileTransferObject = this.transfer.create();

        this.loading = this.loadingCtrl.create({
            content: 'טוען...',
        });
        this.loading.present();

        // Use the FileTransfer to upload the image

        fileTransfer.upload(targetPath, this.ServerUrl+'uploadImage', options).then(data => {
            console.log("Updata  : " , data.response );
            //this.serverImage = data.response;
            this.loading.dismissAll();
            this.uploadedImagePath = data.response;
            this.addChatTitle(1);
        }, err => {
            this.loading.dismissAll();
        });
    }



    takePhoto(sourceType: any) {

        // Create options for the Camera Dialog
        var options = {
            quality: 60,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            targetWidth: 1000,
            targetHeight: 1000,
            allowEdit: true
        };

        // Get the data of an image
        this.camera.getPicture(options).then((imagePath) => {
            // Special handling for Android library
            console.log("f0");
            if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
                console.log("f1");
                this.filePath.resolveNativePath(imagePath)
                    .then(filePath => {
                        this.correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                        this.currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                        this.copyFileToLocalDir(this.correctPath, this.currentName, this.createFileName());
                    });
            } else {
                console.log("f1");
                this.currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                console.log("f2 : " , this.currentName);
                this.correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                console.log("f3 : " , this.correctPath);
                this.copyFileToLocalDir(this.correctPath, this.currentName, this.createFileName());
                console.log("f4");
            }
        }, (err) => {
            //this.presentToast('Error while selecting image.');
        });



    }


    private createFileName() {
        console.log("f1");
        var d = new Date(),
            n = d.getTime(),
            newFileName =  n + ".jpg";
        return newFileName;
    }

    private copyFileToLocalDir(namePath, currentName, newFileName) {
        console.log("f2 : "+ namePath + " : " + currentName + " : " + newFileName);
        this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
            this.lastImage = newFileName;
            this.uploadPhoto();
        }, error => {
            //this.presentToast('Error while storing file.');
        });
    }

    protected autoSize(pixels: number = 0): void {
       // let textArea = this.element.nativeElement.getElementsByTagName('title')[0];
        let textArea = <HTMLElement>document.querySelector(".chatInput")
        console.log(textArea)
        textArea.style.overflow = 'hidden';
        textArea.style.height  = 'auto';
        if (pixels === 0){
            textArea.style.height  = textArea.scrollHeight + 'px';
        } else {
            textArea.style.height  = pixels + 'px';
        }
        return;
    }


    getHour(DateStr)
    {
        let Hour = DateStr.split(" ");
        let Hour1 = String(Hour[0]).split("/");
        return  Hour1[0]+"/"+Hour1[1] + " | " + Hour[1] ;
    }

}
