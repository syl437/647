import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DayAfterPage } from './day-after';

@NgModule({
  declarations: [
    DayAfterPage,
  ],
  imports: [
    IonicPageModule.forChild(DayAfterPage),
  ],
  exports: [
    DayAfterPage
  ]
})
export class DayAfterPageModule {}
