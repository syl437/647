import {Component, OnInit} from '@angular/core';
import { NavController } from 'ionic-angular';
import {RegisterPage} from "../register/register";
import {QuestionsPage} from "../questions/questions";
import {WeightService} from "../../services/weightService";
import {TrainingsSevice} from "../../services/TraingngsService";
import {QuestionService} from "../../services/QuestionService";
import {MealsPage} from "../meals/meals";
import {MealsService} from "../../services/MealsService";
import {Platform} from 'ionic-angular';
import {AboutPage} from "../about/about";
import {LoginPage} from "../login/login";
import {Config} from "../../services/config";
import {ChatSevice} from "../../services/chatService";
import { Push, PushObject, PushOptions }  from "@ionic-native/push";

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage implements OnInit{

    newMessage = 'No Message3';
    identify = window.localStorage.identify;
    Obj:any;
    public screenHeight = screen.height-100+'px';
    public date:any;
    public hours:any;
    public minutes:any;
    public seconds:any;
    public time:any;
    public today:any;
    public dd:any;
    public mm:any;
    public yyyy:any;
    public newdate:any;

    constructor(public navCtrl: NavController,private push: Push, public ChatService:ChatSevice , public Settings:Config,private _platform: Platform, public WeightService:WeightService,public TrainingsService:TrainingsSevice,public QuestionService:QuestionService,public MealsService:MealsService )
    {
        //this.initializeApp();
        //this.pushSetup();
        console.log("Pass : " ,this.identify )
        if(window.localStorage.identify)
            this.Settings.UserId = window.localStorage.identify;
    }

    onGoToRegister() {
        if (this.identify)
        {
            this.navCtrl.push(RegisterPage);
            //this.navCtrl.push(LoginPage);
        }
        else
        {
            this.Settings.UserId = this.identify;
            this.navCtrl.push(LoginPage);
        }
    }

    onGoToQuestion()
    {
        this.navCtrl.push(QuestionsPage);
    }

    onGoToAboutPage()
    {
        this.navCtrl.push(AboutPage)
    }

    ngOnInit()
    {
        this.QuestionService.getAllQuestions('getQuestions').then((data: any) => {});
    }








}