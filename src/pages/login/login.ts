import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {loginService} from "../../services/loginService";
import {Config} from "../../services/config";
import {RegisterPage} from "../register/register";
import {ChatSevice} from "../../services/chatService";

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

    public Identity = '';

  constructor(public navCtrl: NavController, public navParams: NavParams, public Login:loginService, public Settings:Config,public ChatService:ChatSevice) {
      console.log('LoginPage');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  CheckConnect()
  {
      this.Login.getUserDetails('getUserDetails_newVersion' , this.Identity).then((data: any) => {
          if(data != 0)
              this.setUserSettings(data);
          else
              alert("ת.ז. לא תקינה אנא נסה שנית")
      });
  }

  setUserSettings(data)
  {
      window.localStorage.identify = data[0].id;
      window.localStorage.course_id = data[0].course_id;
      window.localStorage.user_image = data[0].image;
      window.localStorage.name = data[0].name;
      window.localStorage.dayAfter = data[0].dayAfter;

      this.Settings.CourseId = data[0].course_id;
      this.Settings.FullName = data[0].name;
      //this.ChatService.registerPush("registerPush" , registration.registrationId)
      this.Settings.UserId = data[0].id;
      console.log("UserId : ", data[0].id)
      this.navCtrl.push(RegisterPage);
  }
}

