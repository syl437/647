import {Component, OnInit} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {MealsService} from "../../services/MealsService";
import {MealsPage} from "../meals/meals";


@IonicPage()
@Component({
    selector: 'page-meals-menu',
    templateUrl: 'meals-menu.html',
})


export class MealsMenuPage implements OnInit {
    
    MealsTypesArray: any;
    MealsArray: any;
    MealsArray1: any;
    MenuLimits: any;
    isDataAvailable: boolean = false;
    currentTab = 0;
    mealNum: number;
    tabNum: number = 0;
    currentLimit = 0;
    currentSelected = 0;
    CheckBox: boolean;
    checkLimit: boolean;
    innerHeight: any;
    public items: any;
    mealName: string = '';
    
    constructor(public navCtrl: NavController, public navParams: NavParams, public MealsService: MealsService, public alertCtrl: AlertController) {
        this.mealNum = navParams.get('mealNum');
    }
    
    ionViewDidLoad() {
        //console.log('ionViewDidLoad MealsMenuPage');
    }
    
    async ngOnInit() {
        
        this.MenuLimits = await this.MealsService.MenuLimits;
        this.isDataAvailable = true;
        this.currentLimit = await this.MealsService.MenuLimits[this.mealNum][0].qnt;
        this.MealsTypesArray = await this.MealsService.getMealsTypesArray();
        this.MealsArray = await this.MealsService.getMealsArray();
        this.items = this.filterItems(this.mealName);
        //this.currentTab = this.MealsTypesArray[0].id;
        
        this.innerHeight = (window.screen.height);
        let elm = <HTMLElement>document.querySelector(".Question");
        elm.style.height = (this.innerHeight - (this.innerHeight * 0.4)) + 'px'
        
    }
    
    changeTab(Id, tabNum) {
        this.tabNum = tabNum;
        this.currentTab = Id;
        this.currentLimit = this.MealsService.MenuLimits[this.mealNum][tabNum].qnt;
        this.items = this.filterItems(this.mealName);
        console.log("Tab : ", this.tabNum + " : " + this.currentTab)
    }
    
    itemClicked(MealsNumber, ItemId, index) {
        console.log("index:",index)
        if (this.MealsArray[this.tabNum][index].selected[this.mealNum].selected == false) {
            this.MealsArray[this.tabNum][index].selected[this.mealNum].qnt = 1;
        }
        //this.checkSelected(index, 0)
    };
    
    
    checkSelected(index, type = null) {
        console.log("CI : ", index)
        this.currentSelected = this.checkMealLimits();
        console.log("currentSelected : ", this.currentSelected);
        
        // if (this.currentSelected > this.currentLimit) {
        //     setTimeout(() => {
        //         if (type == 0) {
        //             console.log("Selected")
        //             this.MealsArray[this.tabNum][index].selected[this.mealNum].qnt = 1;
        //             this.MealsArray[this.tabNum][index].selected[this.mealNum].selected = false;
        //         }
        //     }, 10);
        //     setTimeout(() => {
        //         //alert("עברת את המכסה שלך עבור ארוחה זו");
        //         let alert = this.alertCtrl.create({
        //             //title: 'הגבלת כמות',
        //             subTitle: 'עברת את המכסה שלך עבור ארוחה זו',
        //             buttons: ['סגור']
        //         });
        //         alert.present();
        //
        //     }, 100);
        //     return false;
        // }
        
        return true;
    }
    
    selectedClicked() {
        console.log("Selectedclick : ");
    }
    
    qntAlert(i) {
        // console.log("Place : " , this.MealsArray[this.tabNum][i].selected[this.mealNum].qnt)
        let checked = this.MealsArray[this.tabNum][i].selected[this.mealNum].qnt - 1;
        console.log("CHECKED : ", checked)
        let alert = this.alertCtrl.create();
        alert.setTitle('בחר כמות מנות');
        alert.setCssClass('alertQnt');
        alert.addInput({
            type: 'radio',
            label: 'מנה 1',
            value: '1',
            handler: data => {
                this.MealsArray[this.tabNum][i].selected[this.mealNum].qnt = 1;
            }
        });
        
        alert.addInput({
            type: 'radio',
            label: '2 מנות',
            value: '2',
            handler: data => {
                this.MealsArray[this.tabNum][i].selected[this.mealNum].qnt = 2;
            }
        });
        
        alert.addInput({
            type: 'radio',
            label: '3 מנות',
            value: '3',
            handler: data => {
                this.MealsArray[this.tabNum][i].selected[this.mealNum].qnt = 3;
            }
        });
        
        alert.addInput({
            type: 'radio',
            label: '4 מנות',
            value: '4',
            handler: data => {
                this.MealsArray[this.tabNum][i].selected[this.mealNum].qnt = 4;
            }
        });
        console.log("Alert : ", i)
        alert.data.inputs[checked].checked = true;
        //this.MealsArray[this.tabNum][i].selected[this.mealNum].selected
        //alert.addButton('מחק');

        alert.addButton({
            text: 'מחק',
            handler: data => {
                this.MealsArray[this.tabNum][i].selected[this.mealNum].selected = false;
                this.MealsArray[this.tabNum][i].selected[this.mealNum].qnt = 1;

            }
        });


        alert.addButton({
            text: 'אשר',
            handler: data => {
                this.MealsArray[this.tabNum][i].selected[this.mealNum].qnt = data;
                this.checkLimit = this.checkSelected(i);
                console.log("checkLimit : ", this.checkLimit);
                
                // //if (this.checkLimit == false)
                //     this.MealsArray[this.tabNum][i].selected[this.mealNum].qnt = 1;
                
                return this.checkLimit;
            }
        });
        alert.present();
    }
    
    checkMealLimits() {
        let Selected = 0;
        let mealNum = this.mealNum;
        console.log("MA : ", this.MealsArray, this.MealsService.MenuLimits)
        this.MealsArray[this.tabNum].forEach(function (product, index) {
            if (product.selected[mealNum].selected == true) {
                Selected += Number(product.selected[mealNum].qnt);
            }
        });
        
        return Selected;
    }
    
    // checkLimits() {
    //     let Selected = 0;
    //     let mealNum = this.mealNum;
    //
    //     for (let i = 0; i < this.MealsArray.length; i++) {
    //         Selected = 0;
    //
    //         for (let j = 0; j < this.MealsArray[i].length; j++) {
    //             if (this.MealsArray[i][j].selected[mealNum].selected == true)
    //                 Selected += Number(this.MealsArray[i][j].selected[mealNum].qnt);
    //         }
    //
    //         if (Selected != this.MealsService.MenuLimits[this.mealNum][i].qnt)
    //             return false;
    //     }
    //
    //     return true;
    // }

    checkLimits() {
        let Selected = 0;
        let mealNum = this.mealNum;
        let mealsWarning = [];
        console.log("MA : " , this.MealsArray)

        for (let i = 0; i < this.MealsArray.length; i++) {
            Selected = 0;

            for (let j = 0; j < this.MealsArray[i].length; j++) {
                if (this.MealsArray[i][j].selected[mealNum].selected == true)
                    Selected += Number(this.MealsArray[i][j].selected[mealNum].qnt);
            }

            let obj:any;
            //obj.type = this.MealsArray[i];

            if (Selected == this.MealsService.MenuLimits[this.mealNum][i].qnt)
                obj = 0;
            else if(Selected < this.MealsService.MenuLimits[this.mealNum][i].qnt)
                obj = -1;
            else if(Selected > this.MealsService.MenuLimits[this.mealNum][i].qnt)
                obj = 1;

            mealsWarning.push(obj);
        }

        return mealsWarning;
    }

    
    SaveMeal(MealId) {
        
        let Limits = this.checkLimits();
        let message = '';


        // if(Limits[0] == 1)
        //     message += ' בארוחה זו אכלת פחמימות מעל הכמות הקבועה לך בתפריט ';
        // if(Limits[0] == -1)
        //     message += ' בארוחה זו אכלת פחמימות מתחת הכמות הקבועה לך בתפריט '
        // if(Limits[1] == 1)
        //     message += ' בארוחה זו אכלת חלבונים מעל הכמות הקבועה לך בתפריט ';
        // if(Limits[1] == -1)
        //     message += ' בארוחה זו אכלת חלבונים מתחת הכמות הקבועה לך בתפריט '
        // if(Limits[2] == 1)
        //     message += ' בארוחה זו אכלת שומן מעל הכמות הקבועה לך בתפריט ';
        // if(Limits[2] == -1)
        //     message += ' בארוחה זו אכלת שומן מתחת הכמות הקבועה לך בתפריט ';
        // if(Limits[3] == 1)
        //     message += ' בארוחה זו אכלת פרי מעל הכמות הקבועה לך בתפריט ';
        // if(Limits[3] == -1)
        //     message += ' בארוחה זו אכלת פרי מתחת הכמות הקבועה לך בתפריט ';
        //
        // let alert = this.alertCtrl.create({
        //     title: 'הגבלת כמות',
        //     subTitle: message,
        //     buttons: ['סגור'],
        //     cssClass: 'alertQnt'
        // });
        // alert.present();
    
       
        this.MealsService.SaveMeal('SaveMenu', this.MealsArray, MealId).then((data: any) => {
            this.navCtrl.push(MealsPage, {'mealAccept': this.mealNum})
        })
        // }

        // if (Limits) {
        //
        // }
        // else {
        //     setTimeout(() => {
        //         //alert("עברת את המכסה שלך עבור ארוחה זו");
        //         let alert = this.alertCtrl.create({
        //             title: 'הגבלת כמות',
        //             subTitle: 'לא השלמת את המכסה לארוחה זו , אנא נסה שוב',
        //             buttons: ['סגור'],
        //             cssClass: 'alertQnt'
        //         });
        //         alert.present();
        //
        //     }, 100);
        // }




            // setTimeout(() => {
            //     let str = ''
            //
            //     //alert("עברת את המכסה שלך עבור ארוחה זו");
            //     let alert = this.alertCtrl.create({
            //         title: 'הגבלת כמות',
            //         subTitle: 'לא השלמת את המכסה לארוחה זו , אנא נסה שוב',
            //         buttons: ['סגור'],
            //         cssClass: 'alertQnt'
            //     });
            //     alert.present();
            //
            //     this.MealsService.SaveMeal('SaveMenu', this.MealsArray, MealId).then((data: any) => {
            //         this.navCtrl.push(MealsPage, {'mealAccept': this.mealNum});
            //     });
            // }, 100);

    }
    
    CancelMeal() {
        this.navCtrl.push(MealsPage);
    }
    
    
    setFilteredItems() {
        console.log("Filter : ", this.MealsArray)
        this.items = this.filterItems(this.mealName);
    }
    
    filterItems(searchTerm) {

        if (!searchTerm) searchTerm = '';
        //console.log(searchTerm , this.MealsArray[this.tabNum])
        if (this.MealsArray)
        {
            return this.MealsArray[this.tabNum].filter((item) => {
                return item['title'].toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
            });
        }


    }


}
