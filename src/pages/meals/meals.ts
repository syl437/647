import {Component, OnInit} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {MealsMenuPage} from "../meals-menu/meals-menu";
import {MealsService} from "../../services/MealsService";
import {PopupPage} from "../popup/popup";
import {RegisterPage} from "../register/register";


/**
 * Generated class for the MealsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: 'page-meals',
    templateUrl: 'meals.html',
})
export class MealsPage implements OnInit {
    public MealsTitle = ['ארוחה 1', 'ארוחה 2', 'ארוחה 3', 'ארוחה 4', 'ארוחה 5', 'ארוחה 6'];
    public MealsArray: any[] = [];
    public mealAccept = -1;
    
    constructor(public navCtrl: NavController, public navParams: NavParams, public MealsService: MealsService , public modalCtrl: ModalController) {
        this.mealAccept = navParams.get('mealAccept');
    }
    
    ionViewDidLoad() {
        //console.log('ionViewDidLoad MealsPage');
    }
    
    async ngOnInit() {
        await this.MealsService.getMainPageMeals('getMainPageMenu').then((data: any) => {
            this.MealsArray = data;
            if(this.mealAccept >= 0)
            this.acceptMeal(this.mealAccept)
            console.log("DT : " , data)
        });
    }
    
    gotoMenuMeals(i: number) {
        if(this.MealsArray[i].accept == 0)
        this.navCtrl.push(MealsMenuPage, {mealNum: i});
    }
    
    getCountArray(i) {
        
        if (this.MealsArray[i])
        {
            if(this.MealsArray[i].accept == 1)
            return -1
            else
            return this.MealsArray[i].products.length;
        }
        else
            return 0;
    }
    
    getSelectedProductes(index)
    {
        let str = '';
        
        if(this.MealsArray[index])
        {
            let i=0;
            let Count = this.MealsArray[index].products.length;
            
            for(i=0;i<Count; i++)
            {
                str += this.MealsArray[index].products[i].name;
                if(i != (this.MealsArray[index].products.length-1))
                    str +=  " , ";
            }
            return str;
        }
        else
            return str;
            //return false;
        
    }
    
    acceptMeal(index)
    {
        this.MealsArray[index].accept = 1;
        this.MealsService.acceptMeal('acceptMeal1', index).then((data: any) => {
            console.log("acceptMeal : " , data)
            this.showPopup(index);
        });
    }
    
    showPopup (index)
    {
        let Url:string;
        
        if(index == 0 || index == 2 ||index ==4)
            Url = 'images/popups/1.png';
        else
            Url = 'images/popups/2.png'
        
        
        if(this.MealsArray[0].accept == 1 && this.MealsArray[1].accept == 1 && this.MealsArray[2].accept == 1 && this.MealsArray[3].accept == 1 && this.MealsArray[4].accept == 1 && this.MealsArray[5].accept == 1 )
            Url = 'images/popups/3.png'
        
        let modal = this.modalCtrl.create(PopupPage, {url:Url}, {cssClass: 'FBPage'});
        modal.present();
        modal.onDidDismiss(data => {});
    }
    
    gotoRegister()
    {
        this.navCtrl.push(RegisterPage);
    }
}
