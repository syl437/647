import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';

/**
 * Generated class for the PopupPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-popup',
  templateUrl: 'popup.html',
})
export class PopupPage {

  /*constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PopupPage');
  }*/
    
    img_url:string
    
    constructor(public navCtrl: NavController, public navParams: NavParams , public viewCtrl: ViewController) {
        this.img_url = this.navParams.get('url');
        console.log(this.img_url)
    }
    
    ionViewDidLoad() {
        console.log('ionViewDidLoad PopupPage');
    }
    
    closeModal() {
        this.viewCtrl.dismiss();
    }

}
