import {Component, OnInit} from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {Config} from "../../services/config";
import {QuestionService} from "../../services/QuestionService";
import {HomePage} from '../../pages/home/home';

/**
 * Generated class for the QuestionsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: 'page-questions',
    templateUrl: 'questions.html',
})

export class QuestionsPage implements OnInit {

    QuestionsArray: any[] = [];
    AnswerArray: any[] = [];
    isAnswered: boolean = false;
    QuestionCounter: number = -1;
    Score: number = 0;

    public Contact = {
        'name':'',
        'mail':'',
        'details':'',
        'phone':'',
    }


    constructor(private toastCtrl: ToastController , public navCtrl: NavController, public navParams: NavParams, public Settings: Config, public QuestionService: QuestionService,) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad QuestionsPage');
    }

    ngOnInit() {
        this.QuestionService.getAllQuestions('getQuestions').then((data: any) => {
            console.log("Questions: ", data), this.QuestionsArray = data, this.fillAnswerArray()
        })
        //this.QuestinsArray = this.QuestionService.getQuestionsArray();
        console.log("Questions ", this.QuestionsArray)
    }

    fillAnswerArray() {
        for (var i = 0; i < this.QuestionsArray.length; i++)
            this.AnswerArray.push('');
    }

    questionClicked() {
        this.QuestionCounter++;

        if (this.QuestionCounter == (this.QuestionsArray.length)) {
            this.Score = this.calculateScore();
            this.isAnswered = true;
        }
        console.log('this.AnswerArray : ', this.AnswerArray)
    }

    endQuiz() {
        this.navCtrl.push(HomePage)
    }

    calculateScore() {
        let score = 0;

        for(let i=0;i<this.AnswerArray.length; i++)
        {
            score+= Number(this.AnswerArray[i]);
        }

        return score;
    }

    goBack() {
        //this.navCtrl.setRoot(HomePage);
        this.navCtrl.popToRoot();
    }

    SendForm()
    {

        if(this.Contact.name.length < 3)
            this.presentToast("יש למלא שם מלא", 0)

        else if(this.Contact.mail.length < 3)
            this.presentToast("יש למלא מייל", 0)

        else if(this.Contact.phone.length < 3)
            this.presentToast("יש למלא טלפון", 0)

        else if(this.Contact.details.length < 3)
            this.presentToast("יש למלא תוכן הפנייה", 0)
        else
        {
            //this.Settings.ContactDetails = this.Contact;
            //this.Server.sendContactDetails('getContact');
            this.QuestionService.sendDetails('sendDetails' , this.Contact , this.Score).then((data: any) => {
                console.log("Questions: ", data), this.QuestionsArray = data, this.fillAnswerArray()
            })

            this.presentToast( "תודה נחזור אליך בהקדם", 1)
            this.Contact.name = '';
            this.Contact.mail = '';
            this.Contact.details = '';
            this.Contact.phone = '';
        }
    }


    presentToast(message,type) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'bottom',
            cssClass:'Toast'
        });

        toast.onDidDismiss(() => {
            if(type == 1)
                this.endQuiz();
        });

        toast.present();
    }


}
