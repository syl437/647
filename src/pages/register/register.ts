import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {TrainingsPage} from "../trainings/trainings";
import {WeightPage} from "../weight/weight";
import {MealsPage} from "../meals/meals";
import {HomePage} from "../home/home";
import {loginService} from "../../services/loginService";
import {Config} from "../../services/config";
import {TrainingsSevice} from "../../services/TraingngsService";
import {WeightService} from "../../services/weightService";
import {MealsService} from "../../services/MealsService";
import {ChatPage} from "../chat/chat";
import {LoginPage} from "../login/login";
import {ChatSevice} from "../../services/chatService";
import {DayAfterPage} from "../day-after/day-after";


/**
 * Generated class for the RegisterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: 'page-register',
    templateUrl: 'register.html',
})
export class RegisterPage {

    public ChatMessageCount: any;
    public dayAfter = window.localStorage.dayAfter;


    constructor(public navCtrl: NavController, public navParams: NavParams, public Login: loginService, public Settings: Config, public ChatService: ChatSevice, public WeightService: WeightService, public TrainingsService: TrainingsSevice, public MealsService: MealsService) {
        this.getAllWeight();

        if (!window.localStorage.identify)
            this.navCtrl.push(LoginPage);
    }

    goHome() {
        this.navCtrl.setRoot(HomePage);
    }

    ionViewDidLoad() {
        //console.log('ionViewDidLoad RegisterPage');

    }

    ionViewWillEnter() {
        this.getChatMessagesCount();
    }


    gotoTrainings() {
        this.navCtrl.push(TrainingsPage)
    }

    gotoWeightPage() {
        this.navCtrl.push(WeightPage)
    }

    gotoMeals() {
        if(this.dayAfter == 0)
        this.navCtrl.push(MealsPage)
    }

    gotoChat() {
        this.navCtrl.push(ChatPage)
    }

    gotoDayAfter() {
        if(this.dayAfter == 1)
        this.navCtrl.push(DayAfterPage)
    }

    LogOut() {
        this.Login.GetToken('GetToken','');
        this.Login.logOutUser();
        this.navCtrl.setRoot(HomePage);
    }


    getAllWeight() {
        this.WeightService.getAllWeightByUserid('getWeight').then((data: any) => {
            this.getAllTrainings()
        });
    }

    getAllTrainings() {
        this.TrainingsService.getTrainigTypes().then((data: any) => {
        })
        this.TrainingsService.getAllTrainings('getTraining').then((data: any) => {
            this.getAllMenu()
        })
    }

    getAllMenu() {
        this.MealsService.getMeals('getMenu').then((data: any) => {
        });
        this.MealsService.getMealsTypes('getMenuTypes').then((data: any) => {
        });
        this.MealsService.getMenuLimits('getMenuLimits').then((data: any) => {
        });
    }

    getChatMessagesCount() {
        this.ChatService.getChatMessagesCount('getChatMessagesCount').then((data: any) => {
            this.ChatMessageCount = data;
        });
    }

}
