import {Component, OnInit} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {AddTrainingPage} from "../add-training/add-training";
import {TrainingsSevice} from "../../services/TraingngsService";
import {Config} from "../../services/config";
import {RegisterPage} from "../register/register";


/**
 * Generated class for the TrainingsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: 'page-trainings',
    templateUrl: 'trainings.html',
})


export class TrainingsPage implements OnInit {
    
    TrainingsArray: { id: number, uid: number, date: string, time: string, type: number }[];
    TrainingsTypes: { id: number, title: string }[];
    isDataAvailable: boolean = false;
    
    
    constructor(public navCtrl: NavController, public navParams: NavParams, public TrainingsService: TrainingsSevice, public Settings: Config) {
    }
    
    ionViewDidLoad() {
    }
    
    ngOnInit() {
        //this.TrainingsService.getTrainigTypes().then((data: any) => {this.TrainingsTypes  = data , console.log("Types : " , this.TrainingsTypes[0].title),this.isDataAvailable = true;})
        this.TrainingsService.getAllTrainings('getTraining').then((data: any) => {
            this.TrainingsArray = data , console.log("All: ", this.TrainingsArray)
        })
        this.TrainingsTypes = this.TrainingsService.getTrainigTypesArray()
        //this.TrainingsArray = this.TrainingsService.getAllTrainingsArray()
        console.log("Config : ", this.TrainingsArray)
    }
    
    gotoAddTraining() {
        this.navCtrl.push(AddTrainingPage)
    }
    
    getTrainingType(Id) {
        return this.TrainingsTypes[Id].title;
    }
    
    goRegister() {
        this.navCtrl.push(RegisterPage)
    }
    
    
    changDateDirection(Date) {
        if (Date) {
            Date = Date.split('-')
            Date = Date[2] + '-' + Date[1] + '-' + Date[0];
            return Date;
        }
        
    }
}

