import {Component, OnInit} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {WeightService} from "../../services/weightService";
import {Config} from "../../services/config";

/**
 * Generated class for the WeightPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: 'page-weight',
    templateUrl: 'weight.html',
})
export class WeightPage implements OnInit {

    WeightArray: any;
    TabSelected = 1;
    GroupWeightOpen = 0;
    currentDateWeightArray: any;
    isDataAvailable: boolean = false;
    Uid = this.Settings.UserId;


    constructor(public navCtrl: NavController, public navParams: NavParams, public WeightService: WeightService, public Settings: Config) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad WeightPage');
    }

    ngOnInit() {
        this.WeightArray = this.WeightService.getAllWeightArray();
        this.WeightArray.sort();
        //this.WeightService.getAllWeightByUserid('getWeight').then((data: any) => {this.WeightArray  = data , console.log("Weights : " , this.WeightArray),this.isDataAvailable = true;})
    }

    changeTab(Num) {
        this.TabSelected = Num;

        if (Num == 0)
            this.GroupWeightOpen = 0;
    }

    openGroupPage(j) {
        this.GroupWeightOpen = 1;
        this.currentDateWeightArray = this.WeightArray[j].weights;
        console.log("01 : " , this.currentDateWeightArray)
        this.currentDateWeightArray.sort((a, b) => {
            if (a.weight1 < b.weight1) return -1;
            else if (a.weight1 > b.weight1) return 1;
            else return 0;
        });
        console.log("02 : " , this.currentDateWeightArray)
    }


}
