
import {Http,Headers,Response, RequestOptions} from "@angular/http";
import {Injectable} from "@angular/core";
import { HttpModule } from '@angular/http';
import 'rxjs/Rx';
import {Config} from "./config";


const ServerUrl = "http://tapper.org.il/647/laravel/public/api/";
@Injectable()

export class MealsService
{
    public MealsArray;
    public MealsTypesArray;
    public SelectedArray;
    public Item;
    public Today;
    public date = new Date();
    public isSave;
    public MenuArray:any[]=[];
    public MenuLimits;
    constructor(private http:Http,public Settings:Config) { };
    

    getMeals(url:string)
    {
        this.Today = this.date.getFullYear() + '-' + ('0' + (this.date.getMonth() + 1)).slice(-2) + '-' + ('0' + this.date.getDate()).slice(-2);
        let body = new FormData();
        body.append('uid', this.Settings.UserId.toString());
        body.append('date', this.Today);
        return this.http.post(ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{this.MealsArray = data /*console.log("getMeals : " , data)*/}).toPromise();
    }

    getMealsTypes(url:string)
    {
      //  let body = new FormData();
        // body.append('uid', this.Settings.UserId.toString());

      let body = 'uid=' + this.Settings.UserId.toString();

      let headers = new Headers({
        'Content-Type': 'multipart/form-data'
      });

      let options = new RequestOptions({
        headers: headers
      });

        return this.http.post(ServerUrl + '' + url, body, options).map(res => res.json()).do((data)=>{this.MealsTypesArray = data/*, console.log("getMealsTypes : " , data) */}).toPromise();
    }

    getMealsArray()
    {
        return this.MealsArray;
    }

    getMealsTypesArray()
    {
        return this.MealsTypesArray;
    }

    getMainPageMeals(url)
    {
      this.Today = this.date.getFullYear() + '-' + ('0' + (this.date.getMonth() + 1)).slice(-2) + '-' + ('0' + this.date.getDate()).slice(-2);
      let body = 'uid=' + this.Settings.UserId.toString() + '&date=' + this.Today;

      let headers = new Headers({
        'Content-Type': 'application/x-www-form-urlencoded'
      });

      let options = new RequestOptions({
        headers: headers
      });

      return this.http.post(ServerUrl + '' + url, body, options).map(res => res.json()).do((data:any)=>{ this.MenuArray = data;  }).toPromise();
    }
    
    getMenuLimits(url)
    {
        let body = 'uid=' + this.Settings.UserId.toString();

        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });

        let options = new RequestOptions({
            headers: headers
        });

        return this.http.post(ServerUrl + '' + url, body, options).map(res => res.json()).do((data:any)=>{/*console.log("MenuLimits : " , data, console.log("getMenuLimits : " , data)); */this.MenuLimits = data;}).toPromise();
    }

    ItemClick(MealsNumber,ItemId)
    {
        this.Item = new Object()
        this.Item.MealsNumber = MealsNumber;
        this.Item.ItemId = ItemId;

        this.SelectedArray.push(this.Item);
    }

    SaveMeal(url,Meals,MealId)
    {
        this.SelectedArray = Meals;
        this.Today = this.date.getFullYear() + '-' + ('0' + (this.date.getMonth() + 1)).slice(-2) + '-' + ('0' + this.date.getDate()).slice(-2);

        let body = 'uid=' + this.Settings.UserId.toString() + '&selected=' + JSON.stringify(this.SelectedArray)+ '&date=' + this.Today + '&mealId=' + MealId.toString();
        console.log("SAVVVE " , this.SelectedArray)
        let headers = new Headers({
          'Content-Type': 'application/x-www-form-urlencoded'
        });

        let options = new RequestOptions({
          headers: headers
        });

        return this.http.post(ServerUrl + '' + url, body, options).map(res => res).do((data)=>{this.isSave = data; console.log("SaveMeals : " , data) }).toPromise();
    }
    
    
    acceptMeal(url,index)
    {
        this.Today = this.date.getFullYear() + '-' + ('0' + (this.date.getMonth() + 1)).slice(-2) + '-' + ('0' + this.date.getDate()).slice(-2);
        let ChatDate:any = this.getDate();
        
        let body = 'uid=' + this.Settings.UserId.toString() +  '&date=' + this.Today + '&chatDate=' + ChatDate + '&mealNum=' + index.toString();
        
        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
    
        let options = new RequestOptions({
            headers: headers
        });
    
        return this.http.post(ServerUrl + '' + url, body, options).map(res => res).do((data)=>{this.isSave = data; }).toPromise();
    }

    getDate()
    {
        let date:any = new Date()
        let hours:any = this.date.getHours()
        let minutes:any = this.date.getMinutes()
        let seconds:any = this.date.getSeconds()
    
        if (hours < 10)
            hours = "0" + hours
    
        if (minutes < 10)
            minutes = "0" + minutes
        let time:any = hours + ':' + minutes;
    
    
        let today:any = new Date();
        let dd:any = today.getDate();
        let mm:any = today.getMonth() + 1; //January is 0!
        let yyyy:any = today.getFullYear();
    
        if (dd < 10) {
           dd = '0' + dd
        }
    
        if (mm < 10) {
            mm = '0' + mm
        }
    
        today = dd + '/' + mm + '/' + yyyy;
        let newdate:any = today + ' ' + time;
        return newdate;
    }
    
   
    /*
     addQuoteToFavorites(quote: Quote) {
     this.favoriteQuotes.push(quote);
     console.log(this.favoriteQuotes);
     }
     removeQuoteFromFavorites(quote: Quote) {
     const position = this.favoriteQuotes.findIndex((quoteEl: Quote) => {
     return quoteEl.id == quote.id;
     });
     this.favoriteQuotes.splice(position, 1);
     }

     getFavoriteQuotes() {
     return this.favoriteQuotes.slice();
     }

     isQuoteFavorite(quote: Quote) {
     return this.favoriteQuotes.find((quoteEl: Quote) => {
     return quoteEl.id == quote.id;
     });
     }
     */
};


