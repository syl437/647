import {Http, Headers, Response, RequestOptions} from "@angular/http";
import {Injectable} from "@angular/core";
import {HttpModule} from '@angular/http';
import 'rxjs/Rx';
import {Config} from "./config";


const ServerUrl = "http://tapper.org.il/647/laravel/public/api/";

@Injectable()

export class TrainingsSevice {
    public TrainingsArray;
    public TrainingsTypes;

    constructor(private http: Http, public Settings: Config) {
    };

    getAllTrainings(url) {
        //console.log("TS : " , this.Settings.UserId.toString())
        let body = new FormData();
        body.append('uid', this.Settings.UserId.toString());
        return this.http.post(ServerUrl + '' + url, body).map(res => res.json()).do((data) => {
            this.TrainingsArray = data
        }).toPromise();
    }

    addTraining(url: string, time: string, date: string, type: string) {
        let body = new FormData();
        body.append('date', date);
        body.append('time', time);
        body.append('type', type);
        body.append('uid', this.Settings.UserId.toString());

        //console.log("Body " , type +' :: '+ this.Settings.UserId + " : " ,time);
        return this.http.post(ServerUrl + '' + url, body).map(res => res.json()).do((data) => {
            this.TrainingsArray = data
        }).toPromise();
    }

    getTrainigTypes() {
        let body = new FormData();
        return this.http.post(ServerUrl + 'getTrainigTypes', body).map(res => res.json()).do((data) => {
            this.TrainingsTypes = data
        }).toPromise();
    }

    getTrainigTypesArray() {
        return this.TrainingsTypes;
    }

    getAllTrainingsArray() {
        return this.TrainingsArray;
    }

    checkWeaklyTraining(url: string) {
        let body = new FormData();
        body.append('uid', window.localStorage.identify);
        return this.http.post(ServerUrl + 'checkWeaklyTraining', body).map(res => res.json()).do((data) => {

        }).toPromise();
    }
};


