import {Http,Headers,Response, RequestOptions} from "@angular/http";
import {Injectable, ViewChild} from "@angular/core";
import { HttpModule } from '@angular/http';
import 'rxjs/Rx';
import {Config} from "./config";
import {Subject} from "rxjs/Subject";
import {Observable} from "rxjs/Observable";
import {Content} from "ionic-angular";


const ServerUrl = "http://tapper.org.il/647/laravel/public/api/";
@Injectable()

export class ChatSevice
{
     @ViewChild(Content) content: Content;
    constructor(private http:Http,public Settings:Config) { };

    ChatArray:any[] = [];
    public _ChatArray = new Subject<any>();
    ChatArray$: Observable<any> = this._ChatArray.asObservable();


    addTitle(url:string , title:string,datetime:string,name:string,image:string,chatType:string)
    {
        let body = new FormData();
        body.append('uid', window.localStorage.identify );
        body.append('title', title);
        body.append('type', chatType);
        body.append('course_id', window.localStorage.course_id );
        body.append('date', datetime );
        body.append('name', name );
        body.append('image', image );
        return this.http.post(ServerUrl + '' + url, body).map(res => '').do((data)=>{console.log("Chat : " , data)}).toPromise();
    }

    getChatDetails(url:string)
    {
        let body = new FormData();
        body.append('user_id', window.localStorage.identify.toString() );
        body.append('course_id', window.localStorage.course_id );
        return this.http.post(ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{this.updateChatArray(data)}).toPromise();
    }

    updateChatArray(data)
    {
        this.ChatArray = data;
        this._ChatArray.next({ text: this.ChatArray });
    }

    pushToArray(data)
    {
        //this.ChatArray.push(data);
        this.ChatArray.splice((this.ChatArray.length),0,data);
        this._ChatArray.next(this.ChatArray);
    }

    registerPush(url,pushid)
    {
        let body = new FormData();
        body.append('push_id', pushid.toString() );
        body.append('user_id', window.localStorage.identify.toString() );
        return this.http.post(ServerUrl + '' + url, body).map(res => '').do((data)=>{console.log("chat:" , data)});
    }

    getChatMessagesCount(url)
    {
        let body = new FormData();
        body.append('user_id', window.localStorage.identify.toString() );
        return this.http.post(ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{return data;}).toPromise();
    }
};


