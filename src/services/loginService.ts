
import {Http,Headers,Response, RequestOptions} from "@angular/http";
import {Injectable} from "@angular/core";
import { HttpModule } from '@angular/http';
import 'rxjs/Rx';
import {Config} from "./config";
import {HomePage} from "../pages/home/home";
import {LoadingController} from "ionic-angular";



const ServerUrl = "http://tapper.org.il/647/laravel/public/api/";
@Injectable()

export class loginService
{
    public QuestionsArray;
    public Version = '2';

    constructor(private http:Http,
                public Settings:Config,
                public loadingCtrl: LoadingController,) { };

    getUserDetails(url:string , uid:string)
    {
        let push = '';
        if(window.localStorage.push_id)
            push = window.localStorage.push_id;
        let loading = this.loadingCtrl.create({content: 'Please wait...'});
        loading.present();
        try {
            let body = new FormData();
            body.append('uid', uid);
            body.append('push_id',push);
            return this.http.post(ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{this.QuestionsArray = data}).toPromise();
        } catch (err) {
            console.log( err);
        } finally {
            loading.dismiss();
        }
    }

    GetToken(url,push)
    {
        let loading = this.loadingCtrl.create({content: 'Please wait...'});
        loading.present();

        try {
            let body = new FormData();
            body.append('uid', window.localStorage.identify);
            body.append('token',push);
            body.append('ver','9');
            return this.http.post(ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{}).toPromise();
        } catch (err) {
            console.log( err);
        } finally {
            loading.dismiss();
        }
    }

    logOutUser()
    {
        window.localStorage.identify = '';
        window.localStorage.course_id = '';
        window.localStorage.user_image = '';
        window.localStorage.name = '';
        window.localStorage.dayAfter = '';
        this.Settings.UserId = 0;
        this.Settings.CourseId = '';
        this.Settings.FullName = '';

        localStorage.removeItem("identify");
        localStorage.removeItem("course_id");
        localStorage.removeItem("user_image");
        localStorage.removeItem("name");
        localStorage.removeItem("dayAfter");
        window.localStorage.clear();
    }

    sendToken(url:string , token:string)
    {
        if( window.localStorage.identify )
        {
            console.log("send : " + url + " :"  + token)
            let body = new FormData();
            body.append('uid',  window.localStorage.identify );
            body.append('token',token);
            return this.http.post(ServerUrl + '' + url, body).map(res => res).do((data)=>{}).toPromise();
        }
    }

    checkYesterday(url:string)
    {
        let body = new FormData();
        body.append('uid',  window.localStorage.identify );
        return this.http.post(ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{console.log("Idd : " , window.localStorage.identify)}).toPromise();
    }

};


