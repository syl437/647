
import {Http,Headers,Response, RequestOptions} from "@angular/http";
import {Injectable} from "@angular/core";
import { HttpModule } from '@angular/http';
import 'rxjs/Rx';
import {Config} from "./config";


const ServerUrl = "http://tapper.org.il/647/laravel/public/api/";
@Injectable()

export class WeightService
{
    public WeightArray;


    constructor(private http:Http,public Settings:Config) { };

    getAllWeightByUserid(url:string)
    {
        let body = new FormData();
        //body.append('uid', this.Settings.CourseId.toString());
        body.append('uid', window.localStorage.identify);
        return this.http.post(ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{this.WeightArray = data }).toPromise();
    }

    getAllWeightArray()
    {
      console.log("AAA : " , this.WeightArray)
      return this.WeightArray;
    }

    checkWeight(url)
    {
        let body = new FormData();
        //body.append('uid', this.Settings.CourseId.toString());
        body.append('uid', window.localStorage.identify);
        return this.http.post(ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{}).toPromise();
    }
};


